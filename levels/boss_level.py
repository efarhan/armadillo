'''
Created on 26 oct. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.image_manager import img_manager
from game_object.game_object import GameObject
from game_object.player import Player
from game_object.obstacle import Obstacle
from physics.physics import Physics
from game_object.rainbow_animation import RainbowAnimation

from game_object.mirror import Mirror
from game_object.boss import Boss

class BossLevel(Scene):
    def init(self):
        self.screen_size = get_screen_size()
        self.screen_pos = (0,0)
        self.physics = Physics()
        self.physics.init(0)
        
        self.player = Player(self.physics,pos=(50,50))
        self.player.set_animation(RainbowAnimation(img_manager, self.player.size))
        
        self.mirrors = [Mirror(self.physics, (get_screen_size()[0],30), (0,0)),\
                        Mirror(self.physics, (get_screen_size()[0],30), (0,get_screen_size()[1]-30)),\
                        Mirror(self.physics, (30,get_screen_size()[1]-60), (0,30)),\
                        Mirror(self.physics, (30,get_screen_size()[1]-60), (get_screen_size()[0]-30,30)),]
        
        self.obs = [Obstacle(self.physics, (30,get_screen_size()[1]*3/10), (get_screen_size()[0]*3/10,30)),\
                    Obstacle(self.physics, (get_screen_size()[0]*3/10,30), (30,get_screen_size()[1]*7/10)),\
                    Obstacle(self.physics, (get_screen_size()[0]*3/10,30), (get_screen_size()[0]*7/10-30,get_screen_size()[1]*3/10)),\
                    Obstacle(self.physics, (30,get_screen_size()[1]*3/10), (get_screen_size()[0]*7/10,get_screen_size()[1]*7/10-30)),]
        #self.background = GameObject(self.physics,img_path='data/sprites/rpg_background/background.png',size=(3*480,3*320))
        pygame.mixer.music.load('data/music/BGM_BOSS.ogg')
        pygame.mixer.music.play()
        self.anim_counter = 0
        self.begin = 60
        self.intro = img_manager.load_with_size('data/sprites/text/13.png',\
                        (int(900.0/1000*get_screen_size()[0]),int(303.0/1000*get_screen_size()[1])))
        self.alpha = 0
        self.boss = Boss(self.physics, pos=(get_screen_size()[0]/2, get_screen_size()[1]-30-50),size=(200,100))
        self.end = False
        self.black_screen = pygame.Surface(get_screen_size(), flags=pygame.SRCALPHA)
        self.black_screen.fill(pygame.Color(0,0,0,0))
        self.background = img_manager.load_with_size('data/sprites/background/background_boss.jpg',get_screen_size())
    def loop(self, screen):
        #screen.fill(pygame.Color(255, 255, 255))
        if self.begin > 0 and self.alpha > 0:
            self.alpha -= 2
            if(self.alpha <0):
                self.alpha = 0
        elif self.begin > 0 and self.alpha == 0:
            self.begin-=1
        elif self.begin == 0 and self.alpha >= 0:
            self.alpha += 2
            if(self.alpha >= 255):
                self.alpha = 255
                self.begin = -1
        if self.begin >= 0 and self.alpha >= 0:
            img_manager.show(self.intro, screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
            self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
            screen.blit(self.black_screen,(0,0))
            return
        img_manager.show(self.background, screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
        if not pygame.mixer.music.get_busy():
            pygame.mixer.music.play()
        
        if(pygame.mixer.get_init() and not pygame.mixer.music.get_busy()):
            try:
                pygame.mixer.music.play()
            except pygame.error:
                pass
        self.physics.loop()
        
        #self.background.loop(screen,self.screen_pos)
        self.boss.loop(screen, self.screen_pos)
        for o in self.obs:
            o.loop(screen, self.screen_pos) 
        for m in self.mirrors:
            m.loop(screen,self.screen_pos)

        self.player.loop(screen,self.screen_pos)

        if self.end and self.alpha >=255:
            from engine.level_manager import switch
            switch('last_level')
        elif self.end:
            self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
            screen.blit(self.black_screen,(0,0))
            self.alpha += 2
    def set_orbs_touched(self,a,b):
        self.boss.touched()
    def get_orbs_touched(self):
        return 0  
    def death(self):
        from engine.level_manager import switch
        switch('boss')
