'''
Created on 11 sept. 2013

@author: efarhan
'''
import pygame
from engine.scene import Scene
from game_object.text import Text
from engine.init import get_screen_size
from engine.const import framerate

class TheEnd(Scene):
    def init(self):
        Scene.init(self)
        img_name = ['c0.png','c1.png','c2.png','c3.png','c4.png']
        img_path = 'data/sprites/text/'
        self.imgs = [self.img_manager.load_with_size(img_path+name, \
                        (int(900.0/1000*get_screen_size()[0]),int(303.0/1000.0*get_screen_size()[1]))) for name in img_name]    
        self.index = 0
        self.alpha = 254
        self.black_screen = pygame.Surface(get_screen_size(), flags=pygame.SRCALPHA)
        self.black_screen.fill(pygame.Color(0,0,0,255))
        self.change = False
        
        self.wait = framerate
        self.increase_alpha = self.wait
        self.decrease_alpha = 0
        pygame.mixer.music.load('data/music/BGM_MAIN.ogg')
        pygame.mixer.music.play()
    
    def loop(self, screen):
        
        try:
            self.img_manager.show(self.imgs[self.index], screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
        except IndexError:
            from engine.level_manager import switch
            switch(0)
        if self.alpha <= 0:
            self.alpha = 0
            self.change = True
        elif self.alpha >= 255 and self.change:
            self.alpha = 255
            self.index += 1
            self.change = False
            
        if self.alpha == 0 and self.increase_alpha == 0:
            self.alpha += 1
            self.decrease_alpha = 1
        elif self.alpha == 0:
            self.increase_alpha -= 1
            
        if self.alpha == 255 and self.decrease_alpha == 0:
            self.alpha -= 1
            self.increase_alpha = self.wait
        elif self.alpha == 255:
            self.decrease_alpha -= 1
        
        if self.alpha != 0 and self.alpha != 255:
            if self.increase_alpha == 0:
                self.alpha += 3
            elif self.decrease_alpha == 0:
                self.alpha -= 2
        if self.alpha <= 0:
            self.alpha = 0
            self.change = True
        elif self.alpha >= 255 and self.change:
            self.alpha = 255
            self.index += 1
            self.change = False

        self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
        screen.blit(self.black_screen,(0,0))
            
        