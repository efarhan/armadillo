'''
Created on 27 oct. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.image_manager import img_manager
from engine.const import framerate
from game_object.game_object import GameObject
from game_object.player import Player
from physics.physics import Physics
from game_object.rpg_animation import RPGAnimation
from game_object.rainbow_animation import RainbowAnimation
from game_object.mirror import Mirror
from game_object.orb import Orb
from game_object.obstacle import Obstacle
from game_object.boss_animation import BossAnimation

class LastLevel(Scene):
    def init(self):
        self.screen_size = get_screen_size()
        self.screen_pos = (0,0)
        self.physics = Physics()
        self.physics.init(0)
        
        self.player = Player(self.physics,pos=(get_screen_size()[0]/2,50))
        self.player.boss =True
        self.player.set_animation(BossAnimation(img_manager, self.player.size,player=True))
        
        self.orbs = [Orb(self.physics, 'yellow', (90,90), (get_screen_size()[0]/4,get_screen_size()[1]/4)),\
                     Orb(self.physics, 'red', (90,90), (get_screen_size()[0]*3/4,get_screen_size()[1]/4)),\
                     Orb(self.physics, 'green', (90,90),(get_screen_size()[0]/4,get_screen_size()[1]*3/4)),\
                     Orb(self.physics, 'blue', (90,90), (get_screen_size()[0]*3/4,get_screen_size()[1]*3/4)),]
        self.mirrors = [Mirror(self.physics, (get_screen_size()[0],30), (0,0)),\
                        Mirror(self.physics, (get_screen_size()[0],30), (0,get_screen_size()[1]-30)),\
                        Mirror(self.physics, (30,get_screen_size()[1]-60), (0,30)),\
                        Mirror(self.physics, (30,get_screen_size()[1]-60), (get_screen_size()[0]-30,30)),]
        self.obs = []
        #self.background = GameObject(self.physics,img_path='data/sprites/rpg_background/background.png',size=(3*480,3*320))
        self.anim_counter = 0
        
        self.alpha = 255
        self.end = False
        self.black_screen = pygame.Surface(get_screen_size(), flags=pygame.SRCALPHA)
        self.black_screen.fill(pygame.Color(0,0,0,0))
        self.background = img_manager.load_with_size('data/sprites/background/background_boss.jpg',get_screen_size())
        try:
            pygame.mixer.music.fadeout(1000)
        except:
            pygame.error
        self.wait = framerate*10
        self.decrease_alpha = True
        self.increase_alpha = False
    def loop(self, screen):
        if self.wait == 0 and self.alpha == 255:
            from engine.level_manager import switch
            switch("TheEnd")
        if self.alpha == 0:
            self.decrease_alpha = False
        if self.alpha >= 255:
            self.increase_alpha = False
        if self.wait == 0:
            self.increase_alpha = True
        if self.decrease_alpha:
            self.alpha -=1
        if self.increase_alpha:
            self.alpha += 1
        if self.alpha == 0 and self.wait != 0:
            self.wait -=1
        if self.alpha <= 0:
            self.alpha = 0
            self.decrease_alpha = False
        if self.alpha >= 255:
            self.alpha = 255
            self.increase_alpha = False
        img_manager.show(self.background, screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
        
        self.orb_touched = 0
        self.physics.loop()
        
        #self.background.loop(screen,self.screen_pos)
        for obs in self.obs:
            obs.loop(screen,self.screen_pos)
        for m in self.mirrors:
            m.loop(screen,self.screen_pos)
        for o in self.orbs:
            o.loop(screen, self.screen_pos) 
            o.touched = False
        self.player.loop(screen,self.screen_pos)
        self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
        screen.blit(self.black_screen,(0,0))