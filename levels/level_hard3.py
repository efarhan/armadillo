
'''
Created on 26 oct. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.image_manager import img_manager
from game_object.game_object import GameObject
from game_object.player import Player
from physics.physics import Physics
from game_object.rainbow_animation import RainbowAnimation
from game_object.mirror import Mirror
from game_object.orb import Orb
from game_object.obstacle import Obstacle
from game_object.image import Image

class LevelHard3(Scene):
    def init(self):
        self.screen_size = get_screen_size()
        self.screen_pos = (0,0)
        self.physics = Physics()
        self.physics.init(0)
        
        self.help = Image('help/', (int(514/1000.0*get_screen_size()[0]),int(919/1000.0*get_screen_size()[1])), (200,100))
        self.help_counter = 2*60*60
        
        self.player = Player(self.physics,pos=(get_screen_size()[0]/2,get_screen_size()[1]-50))
        self.player.set_animation(RainbowAnimation(img_manager, self.player.size))
        
        self.mirrors = [Mirror(self.physics, (get_screen_size()[0],30), (0,get_screen_size()[1]-30)),\
                        Mirror(self.physics, (get_screen_size()[0]/4,30), (30,get_screen_size()[1]/4)),\
                        Mirror(self.physics, (get_screen_size()[0]/4,30), (get_screen_size()[0]*3/4-30,get_screen_size()[1]/4)),\
                        Mirror(self.physics, (30,get_screen_size()[1]*3/4-30), (0,get_screen_size()[1]/4)),\
                        Mirror(self.physics, (30,get_screen_size()[1]*3/4-30), (get_screen_size()[0]-30,get_screen_size()[1]/4)),]
        self.orbs = [Orb(self.physics, 'yellow', (90,90), (get_screen_size()[0]*2/3,45)),\
                    Orb(self.physics, 'blue', (90,90), (get_screen_size()[0]/4+50,get_screen_size()[1]*2/3)),\
                    Orb(self.physics, 'red', (90,90), (get_screen_size()[0]-75,get_screen_size()[1]/2)),\
                    Orb(self.physics, 'green', (90,90), (get_screen_size()[0]/8,get_screen_size()[1]/8)),\
                    ]
        self.obs = []
        #self.background = GameObject(self.physics,img_path='data/sprites/rpg_background/background.png',size=(3*480,3*320))
        self.anim_counter = 0
        '''pygame.mixer.init()
        if(not pygame.mixer.music.get_busy()):
            pygame.mixer.music.load('data/music/Tenchi - Mushroom City.ogg')
            pygame.mixer.music.play()
        '''
        self.begin = 60
        self.intro = img_manager.load_with_size('data/sprites/text/11.png',\
                        (int(900.0/1000*get_screen_size()[0]),int(303.0/1000*get_screen_size()[1])))
        self.alpha = 0
        self.end = False
        self.black_screen = pygame.Surface(get_screen_size(), flags=pygame.SRCALPHA)
        self.black_screen.fill(pygame.Color(0,0,0,0))
        self.background = img_manager.load_with_size('data/sprites/background/background5.jpg',get_screen_size())
    def loop(self, screen):
        #screen.fill(pygame.Color(255, 255, 255))
        if self.begin > 0 and self.alpha > 0:
            self.alpha -= 2
            if(self.alpha <0):
                self.alpha = 0
        elif self.begin > 0 and self.alpha == 0:
            self.begin-=1
        elif self.begin == 0 and self.alpha >= 0:
            self.alpha += 2
            if(self.alpha >= 255):
                self.alpha = 255
                self.begin = -1
        if self.begin >= 0 and self.alpha >= 0:
            img_manager.show(self.intro, screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
            self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
            screen.blit(self.black_screen,(0,0))
            return
        img_manager.show(self.background, screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
        
        
        if(not pygame.mixer.music.get_busy()):
            try:
                pygame.mixer.music.play()
            except pygame.error:
                pass
        
        self.orb_touched = 0
        self.physics.loop()
        
        #self.background.loop(screen,self.screen_pos)
        for obs in self.obs:
            obs.loop(screen,self.screen_pos)
        for m in self.mirrors:
            m.loop(screen,self.screen_pos)
        for o in self.orbs:
            o.loop(screen, self.screen_pos) 
            o.touched = False
        self.player.loop(screen,self.screen_pos)
        if self.orb_touched == len(self.orbs) and not self.end:
            
            print self.player.pos
            self.end = True
            self.alpha = 0
        elif(self.alpha <100)and self.orb_touched != len(self.orbs):
            self.end = False
            self.alpha = 0
        if self.end and self.alpha >=255:
            from engine.level_manager import switch
            switch('level_hard4')
        elif self.end:
            self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
            screen.blit(self.black_screen,(0,0))
            self.alpha += 2
        if(self.help_counter == 0):
            self.help.loop(screen, self.screen_pos)
        else:
            self.help_counter -= 1
  
    def death(self):
        from engine.level_manager import switch
        switch('gameplay')
    def get_orbs_touched(self):
        return self.orb_touched
    def set_orbs_touched(self,new_nmb,color):
        for orb in self.orbs:
            if orb.color == color:
                orb.touched = True
                break
        self.orb_touched = new_nmb