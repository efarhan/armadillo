'''
Created on 26 oct. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.image_manager import img_manager
from game_object.game_object import GameObject
from game_object.player import Player
from physics.physics import Physics
from game_object.rpg_animation import RPGAnimation
from game_object.mirror import Mirror
from game_object.orb import Orb
from game_object.obstacle import Obstacle


class RainbowGamePlay(Scene):
    def init(self):
        self.screen_size = get_screen_size()
        self.screen_pos = (0,0)
        self.physics = Physics()
        self.physics.init(0)
        self.player = Player(self.physics, pos=(get_screen_size()[0]/2, get_screen_size()[1]/2))
        self.orb_touched = 0
        self.player.set_animation(RPGAnimation(img_manager, self.player.size))
        self.mirrors = [Mirror(self.physics, (get_screen_size()[0]/4,30), (get_screen_size()[0]/4,0)),\
                        Mirror(self.physics, (get_screen_size()[0]/4-30,30), (get_screen_size()[0]*3/4,0)),\
                        Mirror(self.physics, (30,get_screen_size()[1]/4), (get_screen_size()[0]-30,get_screen_size()[1]/4)),\
                        Mirror(self.physics, (30,get_screen_size()[1]/4-30), (get_screen_size()[0]-30,get_screen_size()[1]*3/4)),\
                        Mirror(self.physics, (get_screen_size()[0]/4-30,30), (30,get_screen_size()[1]-30)),\
                        Mirror(self.physics, (get_screen_size()[0]/4,30), (get_screen_size()[0]/2,get_screen_size()[1]-30)),\
                        Mirror(self.physics, (30,get_screen_size()[1]/4-30), (0,30)),\
                        Mirror(self.physics, (30,get_screen_size()[1]/4), (0,get_screen_size()[1]/2)),]
        self.orbs = [Orb(self.physics, 'yellow', (90,90), (get_screen_size()[0]-45-30,get_screen_size()[1]*3/4)),\
                    Orb(self.physics, 'blue', (90,90), (get_screen_size()[0]/4,get_screen_size()[1]/4)),\
                    Orb(self.physics, 'red', (90,90), (45+30,get_screen_size()[1]*3/4)),\
                    Orb(self.physics, 'green', (90,90), (get_screen_size()[0]/4,get_screen_size()[1]*3/4)),]
        self.obs = [Obstacle(self.physics, (get_screen_size()[0]/4, 30), (0,0)),\
                    Obstacle(self.physics, (get_screen_size()[0]/4, 30), (2*get_screen_size()[0]/4,0)),\
                    Obstacle(self.physics, (30,get_screen_size()[1]/4), (get_screen_size()[0]-30,0)),\
                    Obstacle(self.physics, (30,get_screen_size()[1]/4), (get_screen_size()[0]-30,get_screen_size()[1]/2)),\
                    Obstacle(self.physics, (get_screen_size()[0]/4, 30), (get_screen_size()[0]/4,get_screen_size()[1]-30)),\
                    Obstacle(self.physics, (get_screen_size()[0]/4, 30), (get_screen_size()[0]*3/4,get_screen_size()[1]-30)),\
                    Obstacle(self.physics, (30,get_screen_size()[1]/4), (0,get_screen_size()[1]/4)),\
                    Obstacle(self.physics, (30,get_screen_size()[1]/4), (0,get_screen_size()[1]*3/4)),]
        self.anim_counter = 0

        
    def loop(self, screen):
        #screen.fill(pygame.Color(255, 255, 255))
        
        
        self.physics.loop()
        
        #self.background.loop(screen,self.screen_pos)
        for obs in self.obs:
            obs.loop(screen,self.screen_pos)
        for m in self.mirrors:
            m.loop(screen,self.screen_pos)
        for o in self.orbs:
            o.loop(screen, self.screen_pos) 
        self.player.loop(screen,self.screen_pos)
        
  
    def death(self):
        from engine.level_manager import switch
        switch('gameplay')
    def get_orbs_touched(self):
        return self.orb_touched
    def set_orbs_touched(self,new_nmb):
        self.orb_touched = new_nmb