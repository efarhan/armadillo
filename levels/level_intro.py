'''
Created on 26 oct. 2013

@author: efarhan
'''
'''
Created on 26 oct. 2013

@author: efarhan
'''
import pygame
from engine.init import get_screen_size
from engine.scene import Scene
from engine.image_manager import img_manager
from game_object.game_object import GameObject
from game_object.player import Player
from physics.physics import Physics
from game_object.rpg_animation import RPGAnimation
from game_object.rainbow_animation import RainbowAnimation
from game_object.mirror import Mirror
from game_object.orb import Orb


class IntroLevel(Scene):
    def init(self):
        self.screen_size = get_screen_size()
        self.screen_pos = (0,0)
        self.physics = Physics()
        self.physics.init(0)
        
        self.player = Player(self.physics,pos=(get_screen_size()[0]/2,50))
        self.player.set_animation(RainbowAnimation(img_manager, self.player.size))
        
        self.mirrors = [Mirror(self.physics, (30,get_screen_size()[1]/2), (0,get_screen_size()[1]/4)),\
                       ]
        self.orbs = [Orb(self.physics, 'yellow', (90,90), (get_screen_size()[0]/2,get_screen_size()[1]/2)),]
        #self.background = GameObject(self.physics,img_path='data/sprites/rpg_background/background.png',size=(3*480,3*320))
        self.anim_counter = 0
        self.begin = 60
        
        self.intro = img_manager.load_with_size('data/sprites/text/1.png',\
                        (int(900.0/1000*get_screen_size()[0]),int(303.0/1000*get_screen_size()[1])))
        self.alpha = 254
        self.end = False
        self.black_screen = pygame.Surface(get_screen_size(), flags=pygame.SRCALPHA)
        self.black_screen.fill(pygame.Color(0,0,0,0))
        self.background = img_manager.load_with_size('data/sprites/background/background.jpg',get_screen_size())
    def loop(self, screen):
        #screen.fill(pygame.Color(255, 255, 255))
        if self.begin > 0 and self.alpha > 0:
            self.alpha -= 2
            if(self.alpha <0):
                self.alpha = 0
        elif self.begin > 0 and self.alpha == 0:
            self.begin-=1
        elif self.begin == 0 and self.alpha >= 0:
            self.alpha += 2
            if(self.alpha >= 255):
                self.alpha = 255
                self.begin = -1
        if self.begin >= 0 and self.alpha >= 0:
            img_manager.show(self.intro, screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
            self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
            screen.blit(self.black_screen,(0,0))
            return
        
        if(pygame.mixer.get_init() and not pygame.mixer.music.get_busy()):
            pygame.mixer.music.play()
        img_manager.show(self.background, screen, (get_screen_size()[0]/2,get_screen_size()[1]/2))
        self.orb_touched = 0
        self.physics.loop()
        
        #self.background.loop(screen,self.screen_pos)
        for m in self.mirrors:
            m.loop(screen,self.screen_pos)
        for o in self.orbs:
            o.loop(screen, self.screen_pos) 
            o.touched = False
        self.player.loop(screen,self.screen_pos)
        if self.orb_touched == len(self.orbs) and not self.end:
            self.end = True
            self.alpha = 0
        elif(self.alpha <100)and self.orb_touched != len(self.orbs):
            self.end = False
            self.alpha = 0
        if self.end and self.alpha >=255:
            from engine.level_manager import switch
            switch('intro2')
        elif self.end:
            self.black_screen.fill(pygame.Color(0,0,0,self.alpha))
            screen.blit(self.black_screen,(0,0))
            self.alpha += 2
  
    def death(self):
        from engine.level_manager import switch
        switch('gameplay')
    def get_orbs_touched(self):
        return self.orb_touched
    def set_orbs_touched(self,new_nmb,color):
        for orb in self.orbs:
            if orb.color == color:
                orb.touched = True
                break
        self.orb_touched = new_nmb