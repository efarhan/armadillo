from engine.scene import Scene
from engine.init import get_screen_size
from game_object.text import Text
import pygame
from engine.const import framerate
#font_obj, msg, sound_obj

class Kwakwa(Scene):
	def init(self):
		self.text = self.img_manager.load('data/sprites/text/kwakwa.png')
		self.count = 4*framerate
		pygame.mixer.music.load("data/sound/pissed_off_duck.wav")
		pygame.mixer.music.play()
	def loop(self, screen):
		screen.fill(pygame.Color(255, 255, 255))
		self.img_manager.show(self.text, screen, \
							(get_screen_size()[0]/2,get_screen_size()[1]/2))

		import engine.level_manager as level_manager
			
		if(not pygame.mixer.music.get_busy()):
			pygame.mixer.music.load('data/music/BGM_MAIN.ogg')
			pygame.mixer.music.play()
			level_manager.switch("intro")
