from levels.logo_kwakwa import Kwakwa as logo_kwakwa
from levels.logo_pygame import Pygame as logo_pygame
from levels.main_menu import MainMenu as main_menu
from levels.gameplay import GamePlay as gameplay
from levels.level1 import Level1 as level1
from levels.rpg_gameplay import RPGamePlay as rpg
from levels.theend import TheEnd as the_end
from levels.rainbow_gameplay import RainbowGamePlay as rainbow
from levels.level_intro import IntroLevel as intro
from levels.level_intro_2 import IntroLevel2 as intro2
from levels.level_intro_3 import IntroLevel3 as intro3
from levels.level_intro_4 import CenterLevel as center_level
from levels.level_obs_1 import IntroObsLevel as intro_obs1
from levels.level_obs_2 import IntroObsLevel2 as intro_obs2
from levels.level_obs_3 import IntroObsLevel3 as intro_obs3
from levels.level_obs_4 import IntroObsLevel4 as intro_obs4
from levels.level_hard1 import LevelHard1 as level_hard1
from levels.level_hard2 import LevelHard2 as level_hard2
from levels.level_hard3 import LevelHard3 as level_hard3
from levels.level_hard4 import LevelHard4 as level_hard4
from levels.boss_level import BossLevel as boss_level
from levels.last_level import LastLevel as last_level

dict_level = { "logo_kwakwa" : logo_kwakwa, "logo_pygame" : logo_pygame,\
			 "main_menu" : main_menu, "gameplay": gameplay, "level1":level1,\
			  "TheEnd":the_end, "rpg": rpg, "rainbow":rainbow,\
			   "intro":intro,"intro2":intro2, "intro3":intro3, "intro4":center_level,\
			   "intro_obs1":intro_obs1,"intro_obs2":intro_obs2,"intro_obs3":intro_obs3,"intro_obs4":intro_obs4,\
			   "level_hard1":level_hard1,"level_hard2":level_hard2,"level_hard3":level_hard3,\
			   "level_hard4":level_hard4,"boss":boss_level,"last_level":last_level}

level = 0
def switch(level_name):
	global level
	try:
		level = dict_level[level_name]()
	except KeyError:
		level = 0
	if level != 0:
		level.init()
def function_level():
	if level == 0:
		return level
	return level.loop
