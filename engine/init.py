import pygame, sys

screen_size = (0,0)
def init_screen():
	global screen_size
	screen_info = pygame.display.Info()
	screen_size = (screen_info.current_w, screen_info.current_h)
	if(screen_size[1]>1000):
		screen_size = (1000,1000)
	else:
		screen_size = (screen_size[1]-50,screen_size[1]-50)
	print "Screen size: "+str(screen_size)
	pygame.mouse.set_visible(True)
	pygame.display.set_caption("Trials")
	
	pygame.display.set_icon(pygame.image.load("data/sprites/orb/orbblue01.png"))
	return pygame.display.set_mode(screen_size, )#pygame.FULLSCREEN|pygame.DOUBLEBUF|pygame.HWSURFACE)
def init_joystick():
	pygame.joystick.init()
def get_screen_size():
	global screen_size
	return screen_size

