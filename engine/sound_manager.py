import pygame

class SoundManager():
	def __init__(self):
		sounds = {}
		self.index = 1
		self.index_table={}
		self.sound_table={}
	def load(self, name):
		try:
			self.index_table[name]
		except KeyError:
			s = pygame.mixer.Sound(name)
			self.index_table[name]=self.index
			self.sound_table[self.index]=s
			self.index+=1
			return self.index-1
		return self.index_table[name]
	
	def play(self, index):
		self.sound_table[index].play()
		
snd_manager = SoundManager()