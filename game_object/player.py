'''
Created on 8 sept. 2013

@author: efarhan
'''
import pygame
import math

from game_object import GameObject
from platformer_animation import PlatformerAnimation

from physics.physics import pixel2meter
import engine
from engine.event import get_mouse
from engine.sound_manager import snd_manager
from engine.init import get_screen_size

from physics.contact_listener import RayCastClosestCallback
from physics.physics import meter2pixel, cast_ray_to_screen
from engine.const import invulnerability

class Player(GameObject):
    def __init__(self,physics,move=0,jump=1,factor=1,pos=None):
        GameObject.__init__(self,physics,pos=pos)
        self.size = (int(200/1000.0*get_screen_size()[0]),int(100/1000.0*get_screen_size()[1]))
        self.box_size = (int(25/1000.0*get_screen_size()[0]),int(50/1000.0*get_screen_size()[1]))
        
        if(factor != 1):
            self.size = (factor*self.size[0],factor*self.size[1])
            self.box_size = (factor*self.box_size[0],factor*self.box_size[1])
        self.anim = PlatformerAnimation(self.img_manager,self.size)
        self.anim.jump = jump
        self.anim.load_images()
        self.anim.physics = physics
        self.anim.init_physics(self)
        self.anim.move = move
        #self.pos = (get_screen_size()[0]/2, self.size[1]/2)
        
        self.load_sounds()
        self.callback = RayCastClosestCallback

        self.counter = 0

        self.font = pygame.font.Font('data/font/8-BITWONDER.ttf',25)
        
        self.invulnerability = 0
        self.life = 3
        self.boss = False
    def set_animation(self,anim):
        physics = self.anim.physics
        self.anim = anim
        self.anim.physics = physics
        self.anim.load_images()

    def loop(self, screen,screen_pos,new_size=1):
        if(self.invulnerability > 0):
            self.invulnerability -=1
        if(self.life == 0):
            engine.level_manager.level.death()
        self.anim.loop(self)
        self.counter += 1
        # show the current img
        self.pos = (int(self.pos[0]), int(self.pos[1]))
        '''throw an ray when the player click on the mouse'''
        mouse_click, mouse_pos = get_mouse()
        if mouse_click:
            pos_1 = self.pos
            pos_2 = mouse_pos
            pos_2,d = cast_ray_to_screen(pos_1, pos_2)
            
            '''raycast with physics on objects'''
            callback = self.callback()
            self.physics.cast_ray(callback,pos_1,pos_2)
            if callback.hit:
                contact_pos = callback.point
                pos_2 = (meter2pixel(contact_pos[0]),meter2pixel(contact_pos[1]))
                if (callback.fixture.userData == 5 and not self.boss):
                    n = callback.normal
                    d_product = -d[0]*n[0]+-d[1]*n[1]
                    
                    BA = (pos_2[0]-pos_1[0],pos_2[1]-pos_1[1])
                    v_product = BA[0]*-n[0]+BA[1]*-n[1]
                    BH = (v_product*n[0],v_product*n[1])
                    '''print math.acos(d_product), n, BH, BA'''
                    OH = (pos_2[0]+BH[0],pos_2[1]+BH[1])
                    #pygame.draw.line(screen, pygame.Color(255,255,255),pos_2,OH,5)
                    '''raycast other color rays'''
                    AH = (OH[0]-pos_1[0],OH[1]-pos_1[1])
                    pos_3 = (pos_1[0]+2*AH[0],pos_1[1]+2*AH[1])
                    #pygame.draw.line(screen, pygame.Color(255,255,255),pos_2,pos_3,5)
                    r = get_screen_size()[0]/2
                    AH_norm = ()
                    try:
                        AH_product = math.sqrt(AH[0]**2+AH[1]**2)
                        AH_norm = (AH[0]/AH_product,AH[1]/AH_product)
                    except ZeroDivisionError:
                        AH_norm = (n[1],n[0])
                    pos_red = (int(pos_3[0]+(math.fabs(AH_norm[0])*-r)),int(pos_3[1]+(math.fabs(AH_norm[1])*-r)))
                    pos_red,k = cast_ray_to_screen(pos_2, pos_red)
                    
                    pos_blue = (int(pos_3[0]+(math.fabs(AH_norm[0])*r)),int(pos_3[1]+(math.fabs(AH_norm[1])*r)))
                    pos_blue,k = cast_ray_to_screen(pos_2, pos_blue)
                    
                    pos_yellow = (int(pos_3[0]+(math.fabs(AH_norm[0])*-r/3.0)),int(pos_3[1]+(math.fabs(AH_norm[1])*-r/3)))
                    pos_yellow,k = cast_ray_to_screen(pos_2, pos_yellow)
                    
                    pos_green = (int(pos_3[0]+(math.fabs(AH_norm[0])*r/3.0)),int(pos_3[1]+(math.fabs(AH_norm[1])*r/3.0)))
                    pos_green,k = cast_ray_to_screen(pos_2, pos_green)
                    
                    '''check if it touches an orb'''
                    p = self.cast_color_ray('red', pos_2, pos_red,screen)
                    p = self.cast_color_ray('blue', pos_2, pos_blue,screen)
                    p = self.cast_color_ray('yellow', pos_2, pos_yellow,screen)
                    p = self.cast_color_ray('green', pos_2, pos_green,screen)
            if not self.boss:
                pygame.draw.line(screen, pygame.Color(255,255,255),pos_1,pos_2,5)
            else:
                pygame.draw.line(screen, pygame.Color(0,0,0),pos_1,pos_2,5)
        pass
        
        if(self.invulnerability%2!= 1):
            self.img_manager.show(self.anim.img, screen, (self.pos[0]-screen_pos[0],self.pos[1]+int(10*math.sin(self.counter/10.0))-screen_pos[1]),factor=new_size)
        return self.pos
    def load_sounds(self):
        volume = 0.33
        self.sound0 = snd_manager.load("data/sound/SFX_BAD.ogg")
        snd_manager.sound_table[self.sound0].set_volume(volume)
        self.sound1 = snd_manager.load("data/sound/SFX_GOOD_A.ogg")
        snd_manager.sound_table[self.sound1].set_volume(volume)
        self.sound2 = snd_manager.load("data/sound/SFX_GOOD_B.ogg")
        snd_manager.sound_table[self.sound2].set_volume(volume)
        self.sound3 = snd_manager.load("data/sound/SFX_GOOD_C.ogg")
        snd_manager.sound_table[self.sound3].set_volume(volume)
        self.sound4 = snd_manager.load("data/sound/SFX_GOOD_D.ogg")
        snd_manager.sound_table[self.sound4].set_volume(volume)
    def cast_color_ray(self,color,p1,p2,screen,reflex=2):
        callback = self.callback()
        p = None

        self.physics.cast_ray(callback,p1,p2)
        if callback.hit:
            p3 = (meter2pixel(callback.point[0]),meter2pixel(callback.point[1]))
            if(color == 'red' and callback.fixture.userData == 6):
                snd_manager.play(self.sound1)
                engine.level_manager.level.set_orbs_touched(engine.level_manager.level.get_orbs_touched()+1,color)
            elif(color == 'yellow' and callback.fixture.userData == 7):
                snd_manager.play(self.sound2)
                engine.level_manager.level.set_orbs_touched(engine.level_manager.level.get_orbs_touched()+1,color)
            elif(color == 'green' and callback.fixture.userData == 8):
                snd_manager.play(self.sound3)
                engine.level_manager.level.set_orbs_touched(engine.level_manager.level.get_orbs_touched()+1,color)
            elif(color == 'blue' and callback.fixture.userData == 9):
                snd_manager.play(self.sound4)
                engine.level_manager.level.set_orbs_touched(engine.level_manager.level.get_orbs_touched()+1,color)
            elif(callback.fixture.userData == 5 and reflex>0):
                #mirror
                n = callback.normal
                BA = (p3[0]-p1[0],p3[1]-p1[1])
                v_product = BA[0]*-n[0]+BA[1]*-n[1]
                BH = (v_product*n[0],v_product*n[1])
                '''print math.acos(d_product), n, BH, BA'''
                OH = (p3[0]+BH[0],p3[1]+BH[1])
                #pygame.draw.line(screen, pygame.Color(255,255,255),pos_2,OH,5)
                '''raycast other color rays'''
                AH = (OH[0]-p1[0],OH[1]-p1[1])
                newPos = (p1[0]+2*AH[0],p1[1]+2*AH[1])
                newPos,d = cast_ray_to_screen(p3, newPos)
                self.cast_color_ray(color, p3, newPos, screen, reflex-1)
            p = p3
        else:
            p = None
        if(color == 'red'):
            if(p == None):
                pygame.draw.line(screen, pygame.Color(255,0,0),p1,p2,2)
            else:
                pygame.draw.line(screen, pygame.Color(255,0,0),p1,p,2)
                
        elif(color == 'blue'):
            if(p == None):
                pygame.draw.line(screen, pygame.Color(0,0,255),p1,p2,2)
            else:
                pygame.draw.line(screen, pygame.Color(0,0,255),p1,p,2)
        elif(color == 'yellow'):
            if(p == None):
                pygame.draw.line(screen, pygame.Color(255,255,0),p1,p2,2)
            else:
                pygame.draw.line(screen, pygame.Color(255,255,0),p1,p,2)
        elif(color == 'green'):
            if(p == None):
                pygame.draw.line(screen, pygame.Color(0,255,0),p1,p2,2)
            else:
                pygame.draw.line(screen, pygame.Color(0,255,0),p1,p,2)
    def set_position(self,new_pos):
        self.pos = new_pos
        self.anim.body.position = (pixel2meter(new_pos[0]),pixel2meter(new_pos[1]))
    def touched_by_black_ray(self):
        if(self.invulnerability == 0):
            snd_manager.play(self.sound2)
            self.life-=1
            self.invulnerability = invulnerability