'''
Created on 27 oct. 2013

@author: efarhan
'''
import pygame
import engine
import math
from game_object import GameObject
from physics.physics import cast_ray_to_screen,meter2pixel
from physics.contact_listener import RayCastClosestCallback
from engine.init import get_screen_size
from engine.const import max_reflex, invulnerability
from boss_animation import BossAnimation
from engine.image_manager import img_manager
from orb import Orb
from Box2D import *
from physics.physics import pixel2meter
import random

class OrbBoss(Orb):
    def __init__(self, physics, color, size=None, pos=None):
        Orb.__init__(self, physics, color, size=size, pos=pos)
    def loop(self, screen, screen_pos):
        Orb.loop(self, screen, screen_pos)
    def init_physics(self):
        self.body = self.physics.add_dynamic_object(self)
        
        #add orb sensor
        circle_shape = b2CircleShape(radius=pixel2meter(self.size[0]/3+1))
        fixture_def = b2FixtureDef()
        fixture_def.shape = circle_shape
        fixture_def.density = 1
        fixture_def.isSensor = True
        self.orb_sensor_fixture = self.body.CreateFixture(fixture_def)
        self.set_fixture()
    def change_color(self):
        self.color = random.choice(['red','blue','yellow','green'])
        self.set_fixture()

class Boss(GameObject):
    def __init__(self, physics, img_path='', size=None, pos=None):
        GameObject.__init__(self, physics, img_path=img_path, size=size, pos=pos)
        self.size = (int(200/1000.0*get_screen_size()[0]),int(100/1000.0*get_screen_size()[1]))
        self.box_size = (int(25/1000.0*get_screen_size()[0]),int(50/1000.0*get_screen_size()[1]))
        self.callback = RayCastClosestCallback
        self.anim = BossAnimation(img_manager=img_manager,size=self.size)
        self.anim.physics = self.physics
        self.anim.init_physics(self)
        self.counter = 0
        self.collide = False
        self.orb = OrbBoss(physics, 'red', (90,90), self.pos)
        self.invulnerability = 0
        self.life = 10
    def init_physics(self):
        self.orb.init_physics()
    def load_image(self, img_path, size):
        GameObject.load_image(self, img_path, size)
    def loop(self, screen, screen_pos):
        #GameObject.loop(self, screen, screen_pos)
        if(self.invulnerability > 0):
            self.invulnerability -= 1
        if(self.life == 0):
            engine.level_manager.level.end = True
        self.orb.loop(screen, screen_pos)
        self.counter += 1
        self.anim.loop(self)
        
        newP,k = cast_ray_to_screen(self.pos, (get_screen_size()[0]/2,get_screen_size()[1]/2))
        self.cast_ray(self.pos, newP, screen)
        if(self.invulnerability%2 == 0):
            self.img_manager.show(self.anim.img, screen, (self.pos[0]-screen_pos[0],self.pos[1]+int(10*math.sin(self.counter/10.0))-screen_pos[1]))
    def cast_ray(self,p1,p2,screen,reflex=max_reflex):
        callback = self.callback()
        p = None

        self.physics.cast_ray(callback,p1,p2)
        if callback.hit:
            p3 = (meter2pixel(callback.point[0]),meter2pixel(callback.point[1]))
            if(callback.fixture.userData == 4 and reflex>0):
                #mirror
                n = callback.normal
                BA = (p3[0]-p1[0],p3[1]-p1[1])
                v_product = BA[0]*-n[0]+BA[1]*-n[1]
                BH = (v_product*n[0],v_product*n[1])
                '''print math.acos(d_product), n, BH, BA'''
                OH = (p3[0]+BH[0],p3[1]+BH[1])
                #pygame.draw.line(screen, pygame.Color(255,255,255),pos_2,OH,5)
                '''raycast other color rays'''
                AH = (OH[0]-p1[0],OH[1]-p1[1])
                newPos = (p1[0]+2*AH[0],p1[1]+2*AH[1])
                newPos,d = cast_ray_to_screen(p3, newPos)
                self.cast_ray( p3, newPos, screen, reflex-1)
            elif(callback.fixture.userData == 11):
                #player touched
                engine.level_manager.level.player.touched_by_black_ray()
            p = p3
        else:
            p = None
        if(p == None):
            pygame.draw.line(screen, pygame.Color(0,0,0),p1,p2,5)
        else:
            pygame.draw.line(screen, pygame.Color(0,0,0),p1,p,5)
    def touched(self):
        if(self.invulnerability == 0):
            self.invulnerability = invulnerability
            self.life -= 1
            self.orb.change_color()