'''
Created on 26 oct. 2013

@author: efarhan
'''
import os
from os import listdir
from os.path import isfile, join
import math
from Box2D import *
from engine.init import get_screen_size
from engine.const import animation_step, move
from physics.physics import pixel2meter
from engine.event import get_keys

class BossAnimation():
    def __init__(self,img_manager,size,player=False):
        self.img_manager = img_manager
        self.img = 0
        self.anim_counter = 0
        self.size = size
        self.direction = True
        self.invulnerability = 0
        self.physics = None
        self.animation = 'up'
        self.load_images()
        self.orb_size = (self.size[1],self.size[1])
        self.r = int(200/1000.0*get_screen_size()[0])
        self.theta = 0
        self.period = 240.0
        self.player = player
    def load_images(self):
        img_path = 'data/sprites/boss'
        
        img_files = [ os.path.join(img_path, f) for f in listdir(img_path) if (isfile(join(img_path, f)) and f.find(".png") != -1) ]
        img_files.sort()

        self.move_img = []
        for img in img_files:
            self.move_img.append(self.img_manager.load_with_size(img, self.size))
        '''add orbs behind the boss'''
        self.img = self.move_img[0]
    def loop(self,boss):
        if self.player:
            player = boss
            (self.RIGHT, self.LEFT,self.UP,self.DOWN,self.ACTION) = get_keys()
            
            horizontal, vertical = self.RIGHT-self.LEFT, self.DOWN-self.UP
            
            #set direction and animation
            if(self.direction and horizontal == 0 and vertical != 0):
                self.direction = False
            if(not self.direction and horizontal != 0 and vertical == 0):
                self.direction = True
            
                '''check that the player does not go out of the map'''
                #move horizontally
            if(horizontal == 1 and vertical == 0):
                #move right
                if(player.pos[0]<get_screen_size()[0]-player.size[0]/2):
                    self.physics.move(player,vx=move,vy=0)
                else:
                    self.physics.move(player,vx=0,vy=0)
            elif(horizontal == -1 and vertical == 0):
                if player.pos[0]>player.size[0]/2:
                    self.physics.move(player,vx=-move,vy=0)
                else:
                    self.physics.move(player,vx=0,vy=0)
    
        
            #move vertically
            if(vertical == 1 and horizontal == 0):
    
                if player.pos[1]<get_screen_size()[1]-player.size[1]/2:
                    self.physics.move(player,vy=move,vx=0)
                else:
                    self.physics.move(player,vx=0,vy=0)
            elif(vertical == -1 and horizontal == 0):
    
                if player.pos[1]>player.size[1]/2:
                    self.physics.move(player,vy=-move,vx=0)
                else:
                    self.physics.move(player,vx=0,vy=0)
    
            
            #move diagonally
            if(horizontal != 0 and vertical != 0):
                vx, vy = math.sqrt(2)/2*horizontal*move, math.sqrt(2)/2*vertical*move
                if(vx <0):
                    if player.pos[0]<player.size[0]/2:
                        vx = 0
                elif(vx > 0):
                    if(player.pos[0]>get_screen_size()[0]-player.size[0]/2):
                        vx = 0
                if(vy <0):
                    if player.pos[1]<player.size[1]/2:
                        vy = 0
                elif(vy > 0):
                    if(player.pos[1]>get_screen_size()[1]-player.size[1]/2):
                        vy = 0
                self.physics.move(player,vx=vx, vy=vy)
            elif(horizontal == 0 and vertical == 0):
                self.physics.move(player,0,0)
        else:
            self.theta+=1
            center = (get_screen_size()[0]/2,get_screen_size()[1]/2)
            boss.orb.body.position = (pixel2meter(int(center[0]+self.r*math.sin(self.theta/self.period))), \
                                  pixel2meter(int(center[0]+self.r*math.cos(self.theta/self.period))))
            boss.pos = boss.orb.pos
        self.set_animation()
        
        
    def set_animation(self):
        
        if(self.anim_counter == animation_step):
            anim_index = self.move_img
            try:
                find_index = anim_index.index(self.img)
                if find_index == len(anim_index)-1:
                    self.img = anim_index[0]
                else:
                    self.img = anim_index[find_index+1]
            except ValueError:
                self.img = anim_index[0]
            self.anim_counter = 0
        else:
            self.anim_counter += 1

    def init_physics(self,player):
        pass