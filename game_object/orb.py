'''
Created on 26 oct. 2013

@author: efarhan
'''
from game_object import GameObject
from physics.physics import pixel2meter
from engine.sound_manager import snd_manager
from engine.init import get_screen_size
from Box2D import *
from orb_animation import OrbAnimation

class Orb(GameObject):
    def __init__(self, physics, color, size=None, pos=None):
        newsize = (int(size[0]*get_screen_size()[0]/1000.0),int(size[1]/1000.0*get_screen_size()[1]))
        GameObject.__init__(self, physics, img_path='', size=newsize, pos=pos)
        self.color = color
        
        self.touched = False
        self.init_physics()
        self.anim = OrbAnimation()
        self.anim.load_images(self.size)
    def load_image(self, img_path, size):
        GameObject.load_image(self, img_path, size)
        '''TODO load animation'''
    def set_fixture(self):
        if(self.color == 'red'):
            self.orb_sensor_fixture.userData = 6
        elif(self.color =='yellow'):
            self.orb_sensor_fixture.userData = 7
        elif(self.color == 'green'):
            self.orb_sensor_fixture.userData = 8
        elif(self.color == 'blue'):
            self.orb_sensor_fixture.userData = 9
        else:
            self.orb_sensor_fixture = -1
    def init_physics(self):
        index = self.physics.add_static_circle(self.pos,self.size[0]/3)
        body = self.physics.static_objects[index]
        #add orb sensor
        circle_shape = b2CircleShape(radius=pixel2meter(self.size[0]/3+1))
        fixture_def = b2FixtureDef()
        fixture_def.shape = circle_shape
        fixture_def.density = 1
        fixture_def.isSensor = True
        self.orb_sensor_fixture = body.CreateFixture(fixture_def)
        self.set_fixture()
    def loop(self, screen, screen_pos):
        self.anim.show_image(screen, screen_pos,self.pos, self.color,self.touched)
        '''TODO show animation'''