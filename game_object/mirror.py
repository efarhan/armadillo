'''
Created on 26 oct. 2013

@author: efarhan
'''
import pygame
from game_object import GameObject
from Box2D import *
from physics.physics import pixel2meter

class Mirror(GameObject):
    def __init__(self, physics, size=None, pos=None):
        GameObject.__init__(self, physics, img_path='', size=size, pos=pos)
        self.rect = pygame.rect.Rect(self.pos, self.size)
        self.init_physics()
        self.load_image('data/sprites/mirror/',(30,30))
    def init_physics(self):
        self.physics.add_static_object(self)
        body = self.physics.static_objects[self]
        #add mirror sensor
        polygon_shape = b2PolygonShape()
        polygon_shape.SetAsBox(pixel2meter(self.size[0]/2+1), pixel2meter(self.size[1]/2+1))
        fixture_def = b2FixtureDef()
        fixture_def.shape = polygon_shape
        fixture_def.density = 1
        fixture_def.isSensor = True
        self.mirror_sensor_fixture = body.CreateFixture(fixture_def)
        self.mirror_sensor_fixture.userData = 5
    def load_image(self, img_path, size):
        if(img_path != ''):
            x,y = self.size[0]/30+1, self.size[1]/30+1
            
            if(self.size[0]==30):
                x=1
            elif(self.size[1]==30):
                y=1
            self.img = self.img_manager.load_random_big_image(\
                    [img_path+"mirror1.png",img_path+"mirror2.png",img_path+"mirror3.png",img_path+"mirror4.png"],\
                     size, x,y)
            self.img_manager.images[self.img] = pygame.transform.scale(self.img_manager.images[self.img],self.size)
    def loop(self, screen, screen_pos):
        self.img_manager.show(self.img, screen,self.rect.center)