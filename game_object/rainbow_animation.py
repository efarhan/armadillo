'''
Created on 26 oct. 2013

@author: efarhan
'''
import os
from os import listdir
from os.path import isfile, join
import math

from engine.event import get_keys
from engine.init import get_screen_size
from engine.const import animation_step, move
class RainbowAnimation():
    def __init__(self,img_manager,size):
        self.img_manager = img_manager
        self.img = 0
        self.anim_counter = 0
        self.size = size
        self.direction = True
        self.invulnerability = 0
        self.physics = None
        self.animation = 'up'
    def load_images(self):
        img_path = 'data/sprites/angel'
        
        img_files = [ os.path.join(img_path, f) for f in listdir(img_path) if (isfile(join(img_path, f)) and f.find(".png") != -1) ]
        img_files.sort()

        self.move_img = []
        for img in img_files:
            self.move_img.append(self.img_manager.load_with_size(img, self.size))

        self.img = self.move_img[0]
    def loop(self,player):
        
        #check event
        (self.RIGHT, self.LEFT,self.UP,self.DOWN,self.ACTION) = get_keys()
        
        horizontal, vertical = self.RIGHT-self.LEFT, self.DOWN-self.UP
        
        #set direction and animation
        if(self.direction and horizontal == 0 and vertical != 0):
            self.direction = False
        if(not self.direction and horizontal != 0 and vertical == 0):
            self.direction = True
        
            '''check that the player does not go out of the map'''
            #move horizontally
        if(horizontal == 1 and vertical == 0):
            #move right
            if(player.pos[0]<get_screen_size()[0]-player.size[0]/2):
                self.physics.move(player,vx=move,vy=0)
            else:
                self.physics.move(player,vx=0,vy=0)
        elif(horizontal == -1 and vertical == 0):
            if player.pos[0]>player.size[0]/2:
                self.physics.move(player,vx=-move,vy=0)
            else:
                self.physics.move(player,vx=0,vy=0)

    
        #move vertically
        if(vertical == 1 and horizontal == 0):

            if player.pos[1]<get_screen_size()[1]-player.size[1]/2:
                self.physics.move(player,vy=move,vx=0)
            else:
                self.physics.move(player,vx=0,vy=0)
        elif(vertical == -1 and horizontal == 0):

            if player.pos[1]>player.size[1]/2:
                self.physics.move(player,vy=-move,vx=0)
            else:
                self.physics.move(player,vx=0,vy=0)

        
        #move diagonally
        if(horizontal != 0 and vertical != 0):
            vx, vy = math.sqrt(2)/2*horizontal*move, math.sqrt(2)/2*vertical*move
            if(vx <0):
                if player.pos[0]<player.size[0]/2:
                    vx = 0
            elif(vx > 0):
                if(player.pos[0]>get_screen_size()[0]-player.size[0]/2):
                    vx = 0
            if(vy <0):
                if player.pos[1]<player.size[1]/2:
                    vy = 0
            elif(vy > 0):
                if(player.pos[1]>get_screen_size()[1]-player.size[1]/2):
                    vy = 0
            self.physics.move(player,vx=vx, vy=vy)
        elif(horizontal == 0 and vertical == 0):
            self.physics.move(player,0,0)
        
        if(self.direction):
            if(horizontal == 1):
                self.set_animation('right')
            elif(horizontal == -1):
                self.set_animation('left')
            else:
                self.set_animation('up')
        else:
            if(vertical == 1):
                self.set_animation('up')
            elif(vertical == -1):
                self.set_animation('down')
            else:
                self.set_animation('up')
    def set_animation(self,state):
        self.animation = state
        if(state == 'right'):
            if(self.anim_counter == animation_step):
                anim_index = self.move_img[0:9]
                try:
                    find_index = anim_index.index(self.img)
                    if find_index == len(anim_index)-1:
                        self.img = anim_index[0]
                    else:
                        self.img = anim_index[find_index+1]
                except ValueError:
                    self.img = anim_index[0]
                self.anim_counter = 0
            else:
                self.anim_counter += 1
        elif(state == 'up' or state == 'down'):
            if(self.anim_counter == animation_step):
                anim_index = self.move_img[10:19]
                try:
                    find_index = anim_index.index(self.img)
                    if find_index == len(anim_index)-1:
                        self.img = anim_index[0]
                    else:
                        self.img = anim_index[find_index+1]
                except ValueError:
                    self.img = anim_index[0]
                self.anim_counter = 0
            else:
                self.anim_counter += 1
        elif(state == 'left'):
            if(self.anim_counter == animation_step):
                anim_index = self.move_img[20:29]
                try:
                    find_index = anim_index.index(self.img)
                    if find_index == len(anim_index)-1:
                        self.img = anim_index[0]
                    else:
                        self.img = anim_index[find_index+1]
                except ValueError:
                    self.img = anim_index[0]
                self.anim_counter = 0
            else:
                self.anim_counter += 1
    def init_physics(self,player):
        pass