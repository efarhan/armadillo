'''
Created on 27 oct. 2013

@author: efarhan
'''
from engine.image_manager import img_manager
import os
from os import listdir
from os.path import isfile, join
class OrbAnimation():
    def __init__(self):
        self.img = 0
    def load_images(self,size):
        img_path = 'data/sprites/orb'
        
        img_files = [ os.path.join(img_path, f) for f in listdir(img_path) if (isfile(join(img_path, f)) and f.find(".png") != -1) ]
        img_files.sort()

        self.imgs = [img_manager.load_with_size(name, size) for name in img_files]
        
    def show_image(self,screen,screen_pos,pos,color,animation=False):
        
        if(color == 'blue'):
            self.img = self.imgs[0+animation*4]
        elif(color == 'green'):
            self.img = self.imgs[1+animation*4]
        elif(color == 'red'):
            self.img = self.imgs[2+animation*4]
        elif(color == 'yellow'):
            self.img = self.imgs[3+animation*4]
        
        img_manager.show(self.img, screen, (pos[0] - screen_pos[0], pos[1] - screen_pos[1]))