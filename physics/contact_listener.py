'''
Created on Sep 9, 2013

@author: efarhan
'''
import engine
import math
from Box2D import *

class RayCastClosestCallback(b2RayCastCallback):
    """This callback finds the closest hit"""
    def __repr__(self): return 'Closest hit'
    def __init__(self, **kwargs):
        b2RayCastCallback.__init__(self, **kwargs)
        self.fixture=None
        self.hit=False

    # Called for each fixture found in the query. You control how the ray proceeds
    # by returning a float that indicates the fractional length of the ray. By returning
    # 0, you set the ray length to zero. By returning the current fraction, you proceed
    # to find the closest point. By returning 1, you continue with the original ray
    # clipping. By returning -1, you will filter out the current fixture (the ray
    # will not hit it).
    
    def ReportFixture(self, fixture, point, normal, fraction):
        self.hit=True
        self.fixture=fixture
        self.point=b2Vec2(point)
        self.normal=b2Vec2(normal)
        # You will get this error: "TypeError: Swig director type mismatch in output value of type 'float32'"
        # without returning a value
        return fraction